FROM debian:stable

RUN ["apt-get", "update"]
RUN ["apt-get", "install", "-y", "vim"]
RUN ["apt-get", "install", "-y", "openssh-server"]
RUN ["apt-get", "install", "-y", "cron"]
RUN ["apt-get", "install", "-y", "iptables"]
RUN ["apt-get", "install", "-y", "sudo"]
COPY . init/
RUN ["bash"]
RUN chmod 777 init/*
RUN echo "root:Docker!" | chpasswd
